# -*- coding: utf-8 -*-
"""
Created on Thu Feb 23 00:00:17 2023

@author: Dominik Schwinn
"""

import pandas as pd
import numpy as np



class FAA():
    def __init__(self,
                 verbose=False):
        print(" FAA Wildlife Strikes Analysis (2000-2015) ".center(80,'*'))
        self.df = self.read_faa_dataset()
        self.airport_geo_data = self.read_airport_geo_dataset()
        self.rename_df_columns()
        self.get_time_range_of_dataset()
        self.merge_faa_data_airport_geo_data()
        self.get_number_of_strikes_per_airport()

    def read_faa_dataset(self,
                         verbose=False):
        folder = '../data'
        file = 'faa_data_subset.csv'
        csv_file = folder + '/' + file
        df_raw = pd.read_csv(csv_file,delimiter=';')
        df = df_raw.copy()
        if verbose:
            print(">>> FAA wildlife strikes dataset read")
        return df

    def read_airport_geo_dataset(self,
                                 verbose=False):
        folder = '../data'
        file = 'iata_airport_list.csv'
        csv_file = folder + '/' + file
        df = pd.read_csv(csv_file)
        if verbose:
            print(">>> Airport data set read")
        return df

    # y['Airport: Code'] = [a.strip()[1:] for a in y['Airport: Code']]

    def rename_df_columns(self):
        self.df=self.df.rename(columns={'Airport: Code':'ICAO'})
        print(self.df.columns)

    def get_time_range_of_dataset(self,
                                  verbose=False):
        dates = self.df['Collision Date and Time']
        years = np.array([elem.strip()[6:10] for elem in dates]).astype(int)
        year_min = years.min()
        year_max = years.max()
        if verbose:
            print("FAA data available from {} - {}".format(year_min,year_max))

    def merge_faa_data_airport_geo_data(self):
        print(self.airport_geo_data.columns)
        self.df = pd.merge(self.df,self.airport_geo_data[['ICAO','Latitudes','Longitudes']],how='inner',left_on='ICAO',right_on='ICAO')
        print(self.df.columns)

    def get_number_of_strikes_per_airport(self):
        n_strikes = self.df['ICAO'].value_counts()
        print(" number of strikes per airport ".center(80,'*'))
        z = pd.DataFrame()
        z['ICAO'] = n_strikes.index
        z['n_strikes'] = n_strikes.values
        z = pd.merge(z,self.airport_geo_data[['ICAO','Longitudes','Latitudes','Location']],how='inner',left_on='ICAO',right_on='ICAO')
        z.to_csv('../data/number_strikes_per_airport.csv')

if __name__ == "__main__":
    #
    x = FAA(verbose=True)    